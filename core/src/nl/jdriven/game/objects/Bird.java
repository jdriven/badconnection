package nl.jdriven.game.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import nl.jdriven.game.GameObject;

public class Bird extends GameObject {

    private Texture texture;
    private Animation<TextureRegion> animation;
    private float elapsed_time = 0f;
    private final float width;
    private final float height;

    public Bird(World physicsWorld, float x) {
        super(physicsWorld);
        body.setTransform(x, 3.0f, 0f);
        body.setLinearVelocity(-1f, 0f);
        texture = loadTexture("bird.png");
        TextureRegion[][] textures = TextureRegion.split(texture, texture.getWidth() / 3, texture.getHeight() / 3);
        animation = new Animation<>(1f / 10f, textures[0]);
        animation.setPlayMode(Animation.PlayMode.LOOP);
        width = 0.5f;
        height = 0.5f;
    }

    @Override
    public void update(float delta) {
        elapsed_time += delta;
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(animation.getKeyFrame(elapsed_time), body.getPosition().x, body.getPosition().y, width, height);
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;
        def.fixedRotation = true;
        def.gravityScale = 0f;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        PolygonShape shape = new PolygonShape();
        shape.set(new Vector2[]{
                new Vector2(0f, .3f),
                new Vector2(.1f, .35f),
                new Vector2(.5f, .35f),
                new Vector2(.6f, .3f),
                new Vector2(.5f, .1f),
                new Vector2(.1f, .1f)
        });

        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        fd.density = 1.0f;
        return fd;
    }

    @Override
    public String getFixtureType() {
        return "Enemy";
    }
}
