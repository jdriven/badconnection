package nl.jdriven.game.objects;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

public class Moon implements Disposable {

    private final PointLight pointLight;
    private final PointLight pointLight2;
    private final Texture texture;

    public Moon(RayHandler rayHandler) {
        texture = new Texture("moon.png");
        pointLight = new PointLight(rayHandler, 100, new Color(.4f, .4f, .3f, 1.0f), 1.0f, 0, 4.5f);
        pointLight2 = new PointLight(rayHandler, 500, new Color(.1f, .1f, .05f, 0.5f), 12.0f, 0, 4.5f);
    }

    public void update(float x) {
        pointLight.setPosition(x + 4.5f, pointLight.getY());
        pointLight2.setPosition(x + 4.5f, pointLight.getY());
        pointLight.update();
        pointLight2.update();
    }

    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, pointLight.getX() - .35f, pointLight.getY() - .35f, .7f, 0.7f);
    }

    @Override
    public void dispose() {
        texture.dispose();
    }
}
