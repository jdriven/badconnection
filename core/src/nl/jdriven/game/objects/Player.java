package nl.jdriven.game.objects;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import nl.jdriven.BadConnection;
import nl.jdriven.game.GameObject;

public class Player extends GameObject implements InputProcessor, ContactListener {
    private final float height;
    private final float width;
    private BadConnection badConnection;
    private Camera camera;
    private final Animation<Texture> walkingAnimation;
    private final Animation<Texture> jumpStartAnimation;
    private final Animation<Texture> jumpingAnimation;
    private final Animation<Texture> jumpEndAnimation;
    private Animation<Texture> currentAnimation;
    private float elapsed_time = 0f;
    private boolean isJumping;
    private boolean gameOver;

    public Player(BadConnection badConnection, OrthographicCamera camera, World physicsWorld) {
        super(physicsWorld);
        this.badConnection = badConnection;
        this.camera = camera;
        this.walkingAnimation = createTextureAnimation("dino/Run (%d).png", 8, 1f / 12f, Animation.PlayMode.LOOP);
        this.jumpStartAnimation = createTextureAnimation("dino/JumpStart (%d).png", 2, 1f / 12f, Animation.PlayMode.NORMAL);
        this.jumpingAnimation = createTextureAnimation("dino/Jump (%d).png", 5, 1 / 12f, Animation.PlayMode.LOOP);
        this.jumpEndAnimation = createTextureAnimation("dino/JumpEnd (%d).png", 3, 1f / 12f, Animation.PlayMode.NORMAL);
        this.currentAnimation = walkingAnimation;
        this.height = 1.0f;
        this.width = height * 680f / 472f;
    }

    @Override
    public void update(float delta) {
        body.setLinearVelocity(2f, body.getLinearVelocity().y);
        elapsed_time += delta;
        camera.position.x = body.getPosition().x + 3f;
        if (currentAnimation == jumpStartAnimation) {
            if (currentAnimation.getKeyFrameIndex(elapsed_time) == 1 && !isJumping) {
                body.applyLinearImpulse(new Vector2(0, 5.5f * body.getMass()), body.getLocalCenter(), true);
                isJumping = true;
            } else if (currentAnimation.isAnimationFinished(elapsed_time)) {
                currentAnimation = jumpingAnimation;
                elapsed_time = 0;
            }
        } else if (currentAnimation == jumpEndAnimation && currentAnimation.isAnimationFinished(elapsed_time)) {
            currentAnimation = walkingAnimation;
            elapsed_time = 0;
        }
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(currentAnimation.getKeyFrame(elapsed_time), body.getPosition().x, body.getPosition().y, width, height);
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;
        def.position.set(1f, 1.85f);
        def.fixedRotation = true;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        PolygonShape shape = new PolygonShape();
        shape.set(new Vector2[]{
                new Vector2(.2f, .1f),
                new Vector2(.1f, .3f),
                new Vector2(.5f, .96f),
                new Vector2(.8f, .96f),
                new Vector2(.7f, .1f)
        });

        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        fd.density = 1.0f;
        fd.friction = 0.0f;
        return fd;
    }

    @Override
    public String getFixtureType() {
        return "Player";
    }

    public boolean isGameOver() {
        return gameOver;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.SPACE && !isJumping) {
            currentAnimation = jumpStartAnimation;
            elapsed_time = 0;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.ESCAPE) {
            gameOver = true;
        }
        return false;
    }

    @Override
    public void beginContact(Contact contact) {
        if (isJumping &&
                (("Player".equals(contact.getFixtureA().getUserData()) && "Ground".equals(contact.getFixtureB().getUserData())) ||
                        (("Ground".equals(contact.getFixtureA().getUserData()) && "Player".equals(contact.getFixtureB().getUserData()))))) {
            currentAnimation = jumpEndAnimation;
            elapsed_time = 0;
            isJumping = false;
        }
        if (("Player".equals(contact.getFixtureA().getUserData()) && "Enemy".equals(contact.getFixtureB().getUserData())) ||
                ("Enemy".equals(contact.getFixtureA().getUserData()) && "Player".equals(contact.getFixtureB().getUserData()))) {
            gameOver = true;
        }
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
