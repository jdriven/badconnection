package nl.jdriven.game.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import nl.jdriven.game.GameObject;

public class Cactus extends GameObject {
    private final Texture texture;
    private final float width;
    private final float height;

    public Cactus(World physicsWorld, float x) {
        super(physicsWorld);
        texture = loadTexture("cactus.png");
        body.setTransform(x, 1.75f, 0);
        width = 1.0f;
        height = 1.0f;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, body.getPosition().x, body.getPosition().y, width, height);
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.StaticBody;
        def.fixedRotation = true;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        PolygonShape shape = new PolygonShape();
        shape.set(new Vector2[]{
                new Vector2(.3f, 0f),
                new Vector2(.3f, .8f),
                new Vector2(.45f, .95f),
                new Vector2(.5f, .95f),
                new Vector2(.7f, .65f),
                new Vector2(.6f, 0f)
        });

        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        fd.density = 1.0f;
        return fd;
    }

    @Override
    public String getFixtureType() {
        return "Enemy";
    }
}
