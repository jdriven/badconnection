package nl.jdriven.game.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import nl.jdriven.game.GameObject;

public class Ground extends GameObject {
    private final Texture texture;
    private final float width;
    private final float height;
    private int index;
    private float playerXPos;

    public Ground(World physicsWorld, int index) {
        super(physicsWorld);
        this.index = index;
        texture = loadTexture("ground.png");
        height = 2.1f;
        width = height * texture.getWidth() / texture.getHeight();
        body.setTransform(index * width, body.getPosition().y, 0);
    }

    public void setPlayerXPos(float x) {
        playerXPos = x;
    }

    @Override
    public void update(float delta) {
        if ((index + 1) * width < playerXPos - 1f) {
            index += 2;
            body.setTransform(index * width, body.getPosition().y, 0);
        }
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, body.getPosition().x, body.getPosition().y, width, height);
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.StaticBody;
        def.fixedRotation = true;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        PolygonShape shape = new PolygonShape();
        shape.set(new Vector2[]{
                new Vector2(-0.1f, 0),
                new Vector2(-0.1f, 1.845f),
                new Vector2(0.0f, 1.85f),
                new Vector2(10.24f, 1.85f),
                new Vector2(10.24f + 0.1f, 1.845f),
                new Vector2(10.24f + 0.1f, 0),
        });

        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        fd.density = 1.0f;
        return fd;
    }

    @Override
    public String getFixtureType() {
        return "Ground";
    }
}
