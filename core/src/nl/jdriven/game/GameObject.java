package nl.jdriven.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;
import java.util.List;

public abstract class GameObject implements Disposable {

    protected final Body body;
    protected final World physicsWorld;
    private Fixture fixture;
    private List<Disposable> disposables;

    protected GameObject(World physicsWorld) {
        this.physicsWorld = physicsWorld;
        this.body = createPhysicsBody();
        disposables = new ArrayList<>();
    }

    public final float getXPos() {
        return body.getPosition().x;
    }

    public final void dispose() {
        body.destroyFixture(fixture);
        physicsWorld.destroyBody(body);
        disposables.forEach(Disposable::dispose);
    }

    public final Body createPhysicsBody() {
        Body body = physicsWorld.createBody(getBodyDef());
        createFixture(body);
        return body;
    }

    public final Fixture createFixture(Body body) {
        FixtureDef fixtureDef = getFixtureDef();
        fixture = body.createFixture(fixtureDef);
        fixture.setUserData(getFixtureType());
        fixtureDef.shape.dispose();
        return fixture;
    }

    public final Texture loadTexture(String path) {
        Texture texture = new Texture(path);
        disposables.add(texture);
        return texture;
    }

    public final Animation<Texture> createTextureAnimation(String filenameFormat, int frames, float frameDuration, Animation.PlayMode playMode) {
        Texture[] textures = new Texture[frames];
        for (int i = 0; i < frames; i++) {
            textures[i] = loadTexture(String.format(filenameFormat, i));
        }
        Animation<Texture> animation = new Animation<>(frameDuration, textures);
        animation.setPlayMode(playMode);
        return animation;
    }

    public abstract void update(float delta);

    public abstract void render(SpriteBatch spriteBatch);

    public abstract BodyDef getBodyDef();

    public abstract FixtureDef getFixtureDef();

    public abstract String getFixtureType();
}
