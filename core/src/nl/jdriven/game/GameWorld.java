package nl.jdriven.game;

import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import nl.jdriven.BadConnection;
import nl.jdriven.game.objects.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class GameWorld implements Disposable {

    private final Player player;
    private final Ground ground0;
    private final Ground ground1;
    private final Moon moon;
    private final List<GameObject> gameObjectList;
    private final Random random;
    private float spawnCountDown = 3.0f;

    private final World physicsWorld;
    private final OrthographicCamera camera;
    private final SpriteBatch spriteBatch;
    private final RayHandler rayHandler;

    private BadConnection badConnection;

    public GameWorld(BadConnection badConnection) {
        this.badConnection = badConnection;
        this.random = new Random();
        this.spriteBatch = new SpriteBatch();
        this.physicsWorld = new World(new Vector2(0, -9.81f), false);
        this.rayHandler = new RayHandler(physicsWorld);
        this.rayHandler.setAmbientLight(new Color(.01f, .01f, 0.3f, 0.1f));

        float w = (float) Gdx.graphics.getWidth();
        float h = (float) Gdx.graphics.getHeight();
        this.camera = new OrthographicCamera(8, 8 * (h / w));
        this.camera.position.y = camera.viewportHeight / 2;

        this.player = new Player(badConnection, camera, physicsWorld);
        this.ground0 = new Ground(physicsWorld, 0);
        this.ground1 = new Ground(physicsWorld, 1);
        this.gameObjectList = new ArrayList<>();
        this.moon = new Moon(rayHandler);
        this.physicsWorld.setContactListener(player);
        Gdx.input.setInputProcessor(player);
    }

    public boolean update(float delta) {
        if (player.isGameOver()) {
            if (!physicsWorld.isLocked())
                badConnection.enterMenu();
            return false;
        } else {
            physicsWorld.step(delta, 1, 1);
            destroyOffScreenEnemies();
            spawnNewEnemies(delta);
            updateGameObjects(delta);
            updateScene();
            return true;
        }
    }

    private void destroyOffScreenEnemies() {
        List<GameObject> enemiesToDestroy = gameObjectList.stream()
                .filter(gameObject -> gameObject.getXPos() < player.getXPos() - 2f)
                .collect(Collectors.toList());
        enemiesToDestroy.forEach(gameObject -> {
            gameObjectList.remove(gameObject);
            gameObject.dispose();
        });
    }

    private void spawnNewEnemies(float delta) {
        spawnCountDown -= delta;
        if (spawnCountDown <= 0.0f) {
            spawnCountDown = 2.0f + random.nextFloat() * 2.0f;
            int i = random.nextInt(5);
            if (i == 4) {
                gameObjectList.add(new Bird(physicsWorld, player.getXPos() + 10f));
            } else {
                gameObjectList.add(new Cactus(physicsWorld, player.getXPos() + 10f));
            }
        }
    }

    private void updateGameObjects(float delta) {
        player.update(delta);
        ground0.setPlayerXPos(player.getXPos());
        ground0.update(delta);
        ground1.setPlayerXPos(player.getXPos());
        ground1.update(delta);
        moon.update(player.getXPos());
        gameObjectList.forEach(gameObject -> gameObject.update(delta));
    }

    private void updateScene() {
        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);
        rayHandler.setCombinedMatrix(camera);
        rayHandler.update();
    }

    public void render() {
        Gdx.gl.glClearColor(.5f, .7f, .9f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderGameObjects();
        rayHandler.render();
        renderMoon();
    }

    private void renderGameObjects() {
        spriteBatch.begin();
        ground0.render(spriteBatch);
        ground1.render(spriteBatch);
        gameObjectList.forEach(gameObject -> gameObject.render(spriteBatch));
        player.render(spriteBatch);
        spriteBatch.end();
    }

    private void renderMoon() {
        spriteBatch.begin();
        moon.render(spriteBatch);
        spriteBatch.end();
    }

    @Override
    public void dispose() {
        gameObjectList.forEach(Disposable::dispose);
        ground0.dispose();
        ground1.dispose();
        player.dispose();
        moon.dispose();
        rayHandler.dispose();
        physicsWorld.dispose();
    }
}
