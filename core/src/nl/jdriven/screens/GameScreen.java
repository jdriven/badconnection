package nl.jdriven.screens;

import com.badlogic.gdx.Screen;
import nl.jdriven.BadConnection;
import nl.jdriven.game.GameWorld;

public class GameScreen implements Screen {

    private BadConnection game;
    private GameWorld gameWorld;

    public GameScreen(BadConnection game) {
        this.game = game;
    }

    @Override
    public void show() {
        gameWorld = new GameWorld(game);
    }

    @Override
    public void render(float delta) {
        if (gameWorld.update(delta)) {
            gameWorld.render();
        }
    }

    @Override
    public void hide() {
        gameWorld.dispose();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
