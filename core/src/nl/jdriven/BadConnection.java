package nl.jdriven;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import nl.jdriven.screens.GameScreen;
import nl.jdriven.screens.MenuScreen;

public class BadConnection extends Game {

    private final Screen menuScreen;
    private final Screen gameScreen;

    public BadConnection() {
        menuScreen = new MenuScreen(this);
        gameScreen = new GameScreen(this);
    }

    @Override
    public void create() {
        enterMenu();
    }

    @Override
    public void dispose() {
        menuScreen.dispose();
        gameScreen.dispose();
    }

    public void startGame() {
        setScreen(gameScreen);
    }

    public void enterMenu() {
        setScreen(menuScreen);
    }
}